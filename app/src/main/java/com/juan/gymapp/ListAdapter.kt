package com.juan.gymapp

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListAdapter(val lista:List<String>, val context: Context, val click: Boolean, val clickManager: ClickManager) : RecyclerView.Adapter<ListAdapter.BibliotecaViewHolder>() {

    var clicked = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BibliotecaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_actividad, parent, false) as View
        return BibliotecaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: BibliotecaViewHolder, position: Int) {
        holder.actividadTextView.text = lista[position]
        if(click)
            holder.actividadTextView.setOnClickListener {
                clicked = if (clicked) {
                    holder.actividadTextView.setBackgroundColor(context.getColor(R.color.material_on_primary_emphasis_high_type))
                    clickManager.declick()
                    false
                } else {
                    holder.actividadTextView.setBackgroundColor(context.getColor(R.color.colorPrimary))
                    clickManager.click(lista[position])
                    true
                }

            }
    }

    class BibliotecaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val actividadTextView: TextView

        init {
            actividadTextView = itemView.findViewById(R.id.ActividadTextView)
        }
    }

    interface ClickManager {
        fun click(texto: String)
        fun declick()
    }

}