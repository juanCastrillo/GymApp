package com.juan.gymapp

class Datos() {

    companion object {
        val ActividadesBiblioteca = listOf<String>("Brazos", "Espalda", "Footing", "Hombros", "Piernas", "Pecho")
        val Profesores = listOf<String>("Jose Luis", "Marta", "Antonio")
        val horasDisponiblesPedro = listOf<Boolean>(true, true, true, true, true, true, true, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true)
        val horasDisponiblesMarina = listOf<Boolean>(true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true)
        val horasDisponiblesPaula = listOf<Boolean>(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true)
    }
}