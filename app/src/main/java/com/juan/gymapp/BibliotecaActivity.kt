package com.juan.gymapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton

class BibliotecaActivity : AppCompatActivity() {

    lateinit var bibliotecaRecyclerView: RecyclerView
    lateinit var seleccionarButton: MaterialButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_biblioteca)

        bibliotecaRecyclerView = findViewById(R.id.BibliotecaRecyclerView)
        seleccionarButton = findViewById(R.id.SeleccionarBibliotecaButton)

        bibliotecaRecyclerView.layoutManager = LinearLayoutManager(this)
        bibliotecaRecyclerView.adapter = ListAdapter(Datos.ActividadesBiblioteca, this, true,  object :
            ListAdapter.ClickManager {
                override fun click(activityTitle: String) {
                    startActivity(activityTitle)
                }

            override fun declick() {}
        })


    }

    private fun startActivity(activityTitle: String) {
        val intent = Intent(this, ActividadActivity::class.java)
        intent.putExtra("nombreActividad", activityTitle)
        startActivity(intent)

    }

}