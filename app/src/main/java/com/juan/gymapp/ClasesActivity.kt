package com.juan.gymapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ClasesActivity : AppCompatActivity() {
    lateinit var actividad01: TextView
    lateinit var actividad02: TextView
    lateinit var actividad03: TextView
    lateinit var dia01: TextView
    lateinit var dia02: TextView
    lateinit var dia03: TextView
    lateinit var hora01: TextView
    lateinit var hora02: TextView
    lateinit var hora03: TextView
    lateinit var i: Intent
    lateinit var guardarButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clases)
        i = intent
        actividad01 = findViewById(R.id.actividad01)
        actividad02 = findViewById(R.id.actividad02)
        actividad03 = findViewById(R.id.actividad03)
        dia01 = findViewById(R.id.dia01)
        dia02 = findViewById(R.id.dia02)
        dia03 = findViewById(R.id.dia03)
        hora01 = findViewById(R.id.hora01)
        hora02 = findViewById(R.id.hora02)
        hora03 = findViewById(R.id.hora03)
        val profesor = intent.getStringExtra("Quien").toString()
        when (profesor) {
            "Jose Luis" -> {
                actividad01.text ="Zumba"
                dia01.text ="Lunes"
                hora01.text ="8:00-8:45"
                actividad02.text ="Cardio"
                dia02.text ="Martes"
                hora02.text ="8:00-8:45"
                actividad03.text ="Spinning"
                dia03.text ="Miércoles"
                hora03.text ="8:00-8:45"
            }
            "Marta" -> {
                actividad01.text ="Yoga"
                dia01.text ="Martes"
                hora01.text ="10:00-10:45"
                actividad02.text ="Aerobic"
                dia02.text ="Jueves"
                hora02.text ="10:00-10:45"
                actividad03.text ="Boxeo"
                dia03.text ="Viernes"
                hora03.text ="10:00-10:45"
            }
            "Antonio" -> {
                actividad01.text ="Remo"
                dia01.text ="Lunes"
                hora01.text ="18:00-18:45"
                actividad02.text ="Pilates"
                dia02.text ="Miércoles"
                hora02.text ="18:00-18:45"
                actividad03.text ="Bicicleta"
                dia03.text ="Viernes"
                hora03.text ="18:00-18:45"
            }
        }

        guardarButton = findViewById(R.id.guardarButton)

        guardarButton.setOnClickListener {
            Toast.makeText(applicationContext, "Selección guardada", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, InicioActivity::class.java))
        }

    }
}