package com.juan.gymapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import android.widget.ListView
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.juan.gymapp.ListAdapter.ClickManager
import kotlinx.android.synthetic.main.activity_colectivo.*


class ColectivoActivity : AppCompatActivity() {

    lateinit var selectionButton: MaterialButton
    lateinit var selectorProfesor: RecyclerView
    var profesorElegido:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colectivo)
        selectionButton = findViewById(R.id.selectionButton)

        selectorProfesor = findViewById(R.id.colectivoRecyclerView)
        selectorProfesor.adapter =  ListAdapter(Datos.Profesores, this, true,
            object : ClickManager {
                override fun click(nombreProfesor: String) {
                    profesorElegido = nombreProfesor
                }

                override fun declick() {
                    profesorElegido = null
                }
            })

        selectorProfesor.layoutManager = LinearLayoutManager(this)

        selectionButton.setOnClickListener {
            if(profesorElegido != null)
                abrirProfesor(profesorElegido!!)
        }

    }

    fun abrirProfesor(nombreProfesor: String) {
        val i = Intent(this, ClasesActivity::class.java)
        i.putExtra("Quien", nombreProfesor)
        startActivity(i)
    }
}