package com.juan.gymapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class InicioActivity : AppCompatActivity() {

    lateinit var bibliotecaButton: Button
    lateinit var colectivosButton: Button
    lateinit var individualesButton: Button
    lateinit var consejosButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)

        bibliotecaButton = findViewById(R.id.bibliotecaButton)
        colectivosButton = findViewById(R.id.ejercicoColectivoButton)
        individualesButton = findViewById(R.id.ejercicioIndividualButton)
        consejosButton = findViewById(R.id.consejoButton)

        bibliotecaButton.setOnClickListener {
            startActivity(Intent(this, BibliotecaActivity::class.java))
        }

        colectivosButton.setOnClickListener {
            startActivity(Intent(this, ColectivoActivity::class.java))
        }

        individualesButton.setOnClickListener {
            startActivity(Intent(this, IndividualActivity::class.java))
        }

        consejosButton.setOnClickListener {
            startActivity(Intent(this, ConsejosActivity::class.java))
        }
    }


}