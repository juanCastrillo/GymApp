package com.juan.gymapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ActividadActivity : AppCompatActivity() {

    lateinit var tituloActividad: TextView
    lateinit var titulo: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad)

        tituloActividad = findViewById(R.id.TituloActividad)

        titulo = intent.getStringExtra("nombreActividad").toString()
        tituloActividad.text = titulo

    }
}