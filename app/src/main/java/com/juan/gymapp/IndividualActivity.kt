package com.juan.gymapp

import android.annotation.SuppressLint
import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Toast
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton

class IndividualActivity : AppCompatActivity() {
    lateinit var seleccionProfesor: RadioGroup
    lateinit var Lunes8: ToggleButton
    lateinit var Martes8: ToggleButton
    lateinit var Miercoles8: ToggleButton
    lateinit var Jueves8: ToggleButton
    lateinit var Viernes8: ToggleButton
    lateinit var Sabado8: ToggleButton
    lateinit var Domingo8: ToggleButton
    lateinit var Lunes9: ToggleButton
    lateinit var Martes9: ToggleButton
    lateinit var Miercoles9: ToggleButton
    lateinit var Jueves9: ToggleButton
    lateinit var Viernes9: ToggleButton
    lateinit var Sabado9: ToggleButton
    lateinit var Domingo9: ToggleButton
    lateinit var Lunes10: ToggleButton
    lateinit var Martes10: ToggleButton
    lateinit var Miercoles10: ToggleButton
    lateinit var Jueves10: ToggleButton
    lateinit var Viernes10: ToggleButton
    lateinit var Sabado10: ToggleButton
    lateinit var Domingo10: ToggleButton
    lateinit var Lunes11: ToggleButton
    lateinit var Martes11: ToggleButton
    lateinit var Miercoles11: ToggleButton
    lateinit var Jueves11: ToggleButton
    lateinit var Viernes11: ToggleButton
    lateinit var Sabado11: ToggleButton
    lateinit var Domingo11: ToggleButton
    lateinit var Lunes12: ToggleButton
    lateinit var Martes12: ToggleButton
    lateinit var Miercoles12: ToggleButton
    lateinit var Jueves12: ToggleButton
    lateinit var Viernes12: ToggleButton
    lateinit var Sabado12: ToggleButton
    lateinit var Domingo12: ToggleButton
    lateinit var Lunes13: ToggleButton
    lateinit var Martes13: ToggleButton
    lateinit var Miercoles13: ToggleButton
    lateinit var Jueves13: ToggleButton
    lateinit var Viernes13: ToggleButton
    lateinit var Sabado13: ToggleButton
    lateinit var Domingo13: ToggleButton
    lateinit var Lunes14: ToggleButton
    lateinit var Martes14: ToggleButton
    lateinit var Miercoles14: ToggleButton
    lateinit var Jueves14: ToggleButton
    lateinit var Viernes14: ToggleButton
    lateinit var Sabado14: ToggleButton
    lateinit var Domingo14: ToggleButton
    lateinit var guardarButton: Button

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual)
        seleccionProfesor = findViewById(R.id.RadioGroupProfesor)
        Lunes8 = findViewById(R.id.toggleButtonL8)
        Martes8 = findViewById(R.id.toggleButtonM8)
        Miercoles8 = findViewById(R.id.toggleButtonX8)
        Jueves8 = findViewById(R.id.toggleButtonJ8)
        Viernes8 = findViewById(R.id.toggleButtonV8)
        Sabado8 = findViewById(R.id.toggleButtonS8)
        Domingo8 = findViewById(R.id.toggleButtonD8)
        Lunes9 = findViewById(R.id.toggleButtonL9)
        Martes9 = findViewById(R.id.toggleButtonM9)
        Miercoles9 = findViewById(R.id.toggleButtonX9)
        Jueves9 = findViewById(R.id.toggleButtonJ9)
        Viernes9 = findViewById(R.id.toggleButtonV9)
        Sabado9 = findViewById(R.id.toggleButtonS9)
        Domingo9 = findViewById(R.id.toggleButtonD9)
        Lunes10 = findViewById(R.id.toggleButtonL10)
        Martes10 = findViewById(R.id.toggleButtonM10)
        Miercoles10 = findViewById(R.id.toggleButtonX10)
        Jueves10 = findViewById(R.id.toggleButtonJ10)
        Viernes10 = findViewById(R.id.toggleButtonV10)
        Sabado10 = findViewById(R.id.toggleButtonS10)
        Domingo10 = findViewById(R.id.toggleButtonD10)
        Lunes11 = findViewById(R.id.toggleButtonL11)
        Martes11 = findViewById(R.id.toggleButtonM11)
        Miercoles11 = findViewById(R.id.toggleButtonX11)
        Jueves11 = findViewById(R.id.toggleButtonJ11)
        Viernes11 = findViewById(R.id.toggleButtonV11)
        Sabado11 = findViewById(R.id.toggleButtonS11)
        Domingo11 = findViewById(R.id.toggleButtonD11)
        Lunes12 = findViewById(R.id.toggleButtonL12)
        Martes12 = findViewById(R.id.toggleButtonM12)
        Miercoles12 = findViewById(R.id.toggleButtonX12)
        Jueves12 = findViewById(R.id.toggleButtonJ12)
        Viernes12 = findViewById(R.id.toggleButtonV12)
        Sabado12 = findViewById(R.id.toggleButtonS12)
        Domingo12 = findViewById(R.id.toggleButtonD12)
        Lunes13 = findViewById(R.id.toggleButtonL13)
        Martes13 = findViewById(R.id.toggleButtonM13)
        Miercoles13 = findViewById(R.id.toggleButtonX13)
        Jueves13 = findViewById(R.id.toggleButtonJ13)
        Viernes13 = findViewById(R.id.toggleButtonV13)
        Sabado13 = findViewById(R.id.toggleButtonS13)
        Domingo13 = findViewById(R.id.toggleButtonD13)
        Lunes14 = findViewById(R.id.toggleButtonL14)
        Martes14 = findViewById(R.id.toggleButtonM14)
        Miercoles14 = findViewById(R.id.toggleButtonX14)
        Jueves14 = findViewById(R.id.toggleButtonJ14)
        Viernes14 = findViewById(R.id.toggleButtonV14)
        Sabado14 = findViewById(R.id.toggleButtonS14)
        Domingo14 = findViewById(R.id.toggleButtonD14)
        seleccionProfesor.setOnCheckedChangeListener { radioGroup: RadioGroup, i: Int ->
            Log.d("RadioButtonPressed", i.toString())
            when (i) {
                R.id.radioButtonPedro -> {
                    var cont = 0
                    if(Datos.horasDisponiblesPedro[cont]) {
                        Lunes8.visibility = 0
                    } else {
                        Lunes8.visibility = 4
                    }
                    cont++
                    Martes8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo8.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo9.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo10.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo11.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo12.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo13.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo14.visibility = if(Datos.horasDisponiblesPedro[cont]) {View.VISIBLE} else {View.INVISIBLE}
                }
                R.id.radioButtonMarina -> {
                    var cont = 0
                    if(Datos.horasDisponiblesMarina[cont]) {
                        Lunes8.visibility = 0
                    } else {
                        Lunes8.visibility = 4
                    }
                    cont++
                    Martes8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo8.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo9.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo10.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo11.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo12.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo13.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo14.visibility = if(Datos.horasDisponiblesMarina[cont]) {View.VISIBLE} else {View.INVISIBLE}
                }
                R.id.radioButtonPaula -> {
                    var cont = 0
                    if(Datos.horasDisponiblesPaula[cont]) {
                        Lunes8.visibility = 0
                    } else {
                        Lunes8.visibility = 4
                    }
                    cont++
                    Martes8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo8.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo9.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo10.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo11.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo12.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo13.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Lunes14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Martes14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Miercoles14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Jueves14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Viernes14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Sabado14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                    cont++
                    Domingo14.visibility = if(Datos.horasDisponiblesPaula[cont]) {View.VISIBLE} else {View.INVISIBLE}
                }
            }
        }

        guardarButton = findViewById(R.id.guardarIndvButton)

        guardarButton.setOnClickListener {
            Toast.makeText(applicationContext, "Selección guardada", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, InicioActivity::class.java))
        }
    }
}