package com.juan.gymapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout

class RegisterActivity : AppCompatActivity() {

    lateinit var nameTextField: TextInputLayout
    lateinit var apelliTextField: TextInputLayout
    lateinit var pesoTextField: TextInputLayout
    lateinit var estaturaTextField: TextInputLayout
    lateinit var edadTextField: TextInputLayout
    lateinit var mailTextField: TextInputLayout
    lateinit var contrasenaTextField: TextInputLayout
    lateinit var contrasenav2TextField: TextInputLayout
    lateinit var bancoTextField: TextInputLayout
    lateinit var regisInButton: MaterialButton



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        nameTextField = findViewById(R.id.NameTextField)
        apelliTextField = findViewById(R.id.ApelliTextField)
        pesoTextField = findViewById(R.id.PesoTextField)
        estaturaTextField = findViewById(R.id.EstaturaTextField)
        edadTextField = findViewById(R.id.EdadTextField)
        mailTextField = findViewById(R.id.EmailTextField)
        contrasenaTextField = findViewById(R.id.PasswordTextField)
        contrasenav2TextField = findViewById(R.id.Passwordv2TextField)
        bancoTextField = findViewById(R.id.BancoTextField)
        regisInButton = findViewById(R.id.LoginInButton)


        regisInButton.setOnClickListener {
            // Comprobar si esta vacio alguno de los campos no dejar entrar
            if (nameTextField.editText?.text.toString() != "" && contrasenaTextField.editText?.text.toString() != "") {
                startActivity(Intent(this, InicioActivity::class.java))
            }

        }

    }
}