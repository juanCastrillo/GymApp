package com.juan.gymapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import android.widget.Button

class LoginActivity : AppCompatActivity() {

    lateinit var usernameTextField: TextInputLayout
    lateinit var contrasenaTextField: TextInputLayout
    lateinit var loginInButton: MaterialButton
    lateinit var goingBackButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        usernameTextField = findViewById(R.id.UsernameTextField)
        contrasenaTextField = findViewById(R.id.ContrasenaTextField)
        loginInButton = findViewById(R.id.LoginInButton)

        loginInButton.setOnClickListener {
            // Comprobar si esta vacio alguno de los campos no dejar entrar
            if (usernameTextField.editText?.text.toString() != "" && contrasenaTextField.editText?.text.toString() != "") {
                startActivity(Intent(this, InicioActivity::class.java))
            }

        }

    }



}